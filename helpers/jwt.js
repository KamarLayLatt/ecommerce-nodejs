// const jwt = require("jsonwebtoken");
// const authJwt = (req, res, next) => {
//   try {
//     let token = req.headers.authorization;
//     if (token) {
//       token = token.split(" ")[1];
//       let user = jwt.verify(token, process.env.secret);
//       req.userId = user.id;
//     } else {
//       res.status(401).json({ message: "Unauthorized user" });
//     }
//     next();
//   } catch (error) {
//     res
//       .status(401)
//       .json({ message: "Unautorized User " + req.headers.authorization });
//   }
// };

// module.exports = authJwt;

var { expressjwt: jwt } = require("express-jwt");

// const api = process.env.API_URL;

async function isRevoked(req, payload, done) {
  console.log(payload);
  if (!payload.payload.isAdmin) {
    // done(null, true);
    console.log("not Admin");
    return true;
  }
  console.log("yes Admin");
  //   done();
  return false;
}

module.exports = jwt({
  secret: process.env.secret,
  algorithms: ["HS256"],
  isRevoked: isRevoked,
}).unless({
  path: [
    {
      url: /\/public\/uploads(.*)/,
      method: ["GET", "OPTIONS"],
    },
    {
      url: /\/api\/v1\/products(.*)/,
      method: ["GET", "OPTIONS"],
    },
    {
      url: /\/api\/v1\/categories(.*)/,
      method: ["GET", "OPTIONS"],
    },
    "/api/v1/users/login",
    "api/v1/users/register",
  ],
});
