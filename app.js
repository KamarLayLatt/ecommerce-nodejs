const express = require("express");
const app = express();
const bodyParser = require("body-parser"); //req body
const morgan = require("morgan"); //middleware
const mongoose = require("mongoose"); //database
const cors = require("cors"); //cors not allow

require("dotenv/config");
const api = process.env.API_URL;
const productsRoutes = require("./routers/products");
const categoriesRoutes = require("./routers/categories");
const usersRoutes = require("./routers/users");
const authJwt = require("./helpers/jwt");
const errorHandler = require("./helpers/error-handler");
const ordersRoutes = require("./routers/orders");

app.use(cors());
app.options("*", cors());

//middleware
app.use(bodyParser.json()); //request body
app.use(morgan("tiny")); //middleware library`
app.use(authJwt);
app.use("/public/uploads", express.static(__dirname + "/public/uploads"));
app.use(errorHandler);

//Routers
app.use(`${api}/products`, productsRoutes);
app.use(`${api}/categories`, categoriesRoutes);
app.use(`${api}/users`, usersRoutes);
app.use(`${api}/orders`, ordersRoutes);

//Database
mongoose
  .connect(process.env.CONNECTION_STRING, {
    useNewURLParser: true,
    useUnifiedTopology: true,
    dbName: "eshop-database",
  })
  .then(() => {
    console.log("Database Connection is ready ...");
  })
  .catch((err) => {
    console.log(err);
  });

//Server
app.listen(3000, () => {
  console.log(api);
  console.log("server is running http://localhost:3000");
});
